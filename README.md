# Go Samples

Sample go code / tools written for practice & knowledge gain.

## Enhanced Which

A simple program to search the PATH for a file set, and return the first or all of the executables.

### Usage Example

```shell
$ go run cmd/ewhich/ewhich.go --all -f go
```
