package ewhich

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/akamensky/argparse"
)

func createArgParser() *argparse.Parser {
	return argparse.NewParser("ewhich", "Enhanced which tool")
}

func Run() {
	parser := createArgParser()
	all := parser.Flag("a", "all", &argparse.Options{Help: "Searches for all instances of programs in the path."})
	help := parser.Flag("h", "help", &argparse.Options{Help: "Displays the tool help."})
	files := parser.StringList("f", "file", &argparse.Options{Help: "A filename to search for in the path.", Required: true})

	if err := parser.Parse(os.Args); err != nil {
		fmt.Println(fmt.Errorf("error parsing command line arguments:  %s", err.Error()))
		os.Exit(-1)
	}

	if *help {
		return
	}
	pathsVar := os.Getenv("PATH")
	paths := filepath.SplitList(pathsVar)

	for _, file := range *files {
		fmt.Printf("%s:\n", file)

		for _, path := range paths {
			fullpath := filepath.Join(path, file)
			fileInfo, err := os.Stat(fullpath)
			if err == nil {
				mode := fileInfo.Mode()

				if mode.IsRegular() {
					if mode&0111 != 0 {
						fmt.Println(fullpath)
						if !*all {
							break
						}
					}
				}
			}
		}
	}
}
